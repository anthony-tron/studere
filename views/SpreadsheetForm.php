<form
    method="<?= $method ?>"
    action="<?= $action ?>"
    enctype="multipart/form-data"
>
    <header class="form-title">
        Distribute groups
    </header>
    <label class="form-field form-label">
        Students per group
        <input
            required
            type="number"
            name="group-size"
            min="2"
            step="1"
            placeholder="5"
        />
    </label>
    <label class="form-field form-label">
        Spreadsheet
        <input
            required
            type="file"
            name="spreadsheet"
        />
    </label>
    <fieldset class="form-field">
        <legend class="form-label">Format</legend>
        <label>
            <input
                type="radio"
                name="format"
                value="compact"
                checked
            />
            Compact
        </label>
        <label>
            <input
                type="radio"
                name="format"
                value="chunked"
            />
            Grouped
        </label>
    </fieldset>
    <button
        class="form-submit"
        type="submit"
    >
        Submit
    </button>
</form>

