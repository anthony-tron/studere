<!doctype html>
<html lang="en">
<head>
    <title><?= $title ?></title>
    <link rel="stylesheet" href="/index.css" />
    <meta charset="utf-8" />
    <script type="module" src="/index.js"></script>
</head>
<body>
    <header class="header">
        <nav class="container">
            <a class="home" href="/">Studere</a>
        </nav>
    </header>
    <main class="container">
        <?= $body ?>
    </main>
</body>
</html>

