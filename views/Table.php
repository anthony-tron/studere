<table class="sortable table">
    <thead class="table-header">
<?php foreach($headers as $header): ?>
        <th class="table-cell"><?= $header ?></th>
<?php endforeach; ?>
    </thead>
    <tbody>
<?php foreach($rows as $row): ?>
        <tr class="table-row">
<?php   foreach($row as $value): ?>        
            <td class="table-cell"><?= $value ?></td>
<?php   endforeach; ?>
        </tr>
<?php endforeach; ?>
    </tbody>
</table>

