<?php

require_once 'vendor/autoload.php';

rotor\Schema::route(
    new app\Schema(),
    $_SERVER['REQUEST_URI'],
    $_SERVER['REQUEST_METHOD']
);
