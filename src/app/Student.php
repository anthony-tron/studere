<?php

namespace app;

class Student {

    public function __construct (
        private $firstName,
        private $lastName,
        private $title,
    ) {}

    public function getFirstName() {
        return $this->firstName;
    }

    public function getLastName() {
        return $this->lastName;
    }

    public function getTitle() {
        return $this->title;
    }
}

