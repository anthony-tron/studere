<?php

namespace app;

class ChunkedDistributedGroupsView extends DistributedGroupsView {

    public function computeChunks(array $students, array $groups) {
        $chunks = [];

        for ($i = 0; $i < count($students); ++$i) {
            $chunks[$groups[$i]][] = $students[$i];
        }

        return $chunks;
    }

    public function render(): void {

        $chunks = $this->computeChunks($this->getStudents(), $this->getGroups());

        foreach ($chunks as $group => $students) {
            echo \viewer\View::fromFile('views/Table.php', [
                'headers' => ['Title', 'First name', 'Last name'],
                'rows' => array_map(fn($student) => [
                    $student->getTitle(),
                    $student->getLastName(),
                    $student->getFirstName(),
                ], $students),
            ]);
        }
    }
}


