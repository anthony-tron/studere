<?php

namespace app;

use Box\Spout\Reader\Common\Creator\ReaderEntityFactory;

class StudentModel {

    /**
      * @param fileName basename of the file used to guess spreadsheet format
      * @param $filePath path to file to read
      * @return array of Student
      */
    public function fromSpreadsheet($fileName, $filePath) {
        $reader = ReaderEntityFactory::createReaderFromFile($fileName);
        $reader->open($filePath);

        $sheetIterator = $reader->getSheetIterator();
        $sheetIterator->rewind();
        $sheet = $sheetIterator->current();

        $rowIterator = $sheet->getRowIterator();
        $rowIterator->rewind();

        while ($rowIterator->valid()) {
            $row = $rowIterator->current();
            $cells = $row->getCells();

            $values = \array_map(function($e) { return $e->getValue(); }, $cells);

            $indexes = [
                \array_search('Civilité', $values),
                \array_search('Nom', $values),
                \array_search('Prénom', $values),
            ];

            $rowIterator->next();

            if (\count(\array_filter($indexes, fn($e) => $e !== false)) === 3) {
                break;
            }

        }

        if (empty($indexes)) {
            throw new RuntimeException();
        }

        $students = [];

        while ($rowIterator->valid()) {
            $currentRow = $rowIterator->current();

            $cells = $currentRow->getCells();
            [$title, $firstName, $lastName] = [
                $cells[$indexes[0]]->getValue(),
                $cells[$indexes[1]]->getValue(),
                $cells[$indexes[2]]->getValue(),
            ];

            if ($title && $firstName && $lastName) {
                $students[] = new Student(
                    $firstName,
                    $lastName,
                    $title,
                );
            }

            $rowIterator->next();
        }

        return $students;
    }
}

