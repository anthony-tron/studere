<?php

namespace app;

abstract class DistributedGroupsView extends \viewer\View {
    public function __construct(
        private $students,
        private $groups,
    )
    {}

    public function getStudents() {
        return $this->students;
    }

    public function getGroups() {
        return $this->groups;
    }
}

