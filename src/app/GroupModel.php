<?php

namespace app;

class GroupModel {
    /**
     * creates an array and evenly distribute integers
     * @param min minimum integer to generate
     * @param max maximum integer to generate
     * @param count size of the array to return
     * @return array
     */
    public function distributeIntegers($min, $max, $count) {
        $numbers = [];

        for($i = 0; $i < $count; ++$i) {
            $numbers[] = ($i % ($max - $min + 1)) + $min;
        }

        return $numbers;
    }

    /**
     * creates an array of count($elements) and evenly randomly attribute assign group IDs
     * @param elements to attribute the groups to
     * @param groupSize ideal size of each group
     * @return array of the same size than elements' size
     */
    public function attributeGroups($elements, $groupSize) {
        $groupsCount = (int) ceil(count($elements) / $groupSize);
        $groups = $this->distributeIntegers(1, $groupsCount, count($elements));
        shuffle($groups);
        return $groups;
    }
}

