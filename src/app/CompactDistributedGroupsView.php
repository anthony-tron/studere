<?php

namespace app;

class CompactDistributedGroupsView extends DistributedGroupsView {

    public function render(): void {

        echo \viewer\View::fromFile('views/Table.php', [
            'headers' => ['Title', 'First name', 'Last name', 'Group'],
            'rows' => array_map(fn($student, $group) => [
                $student->getTitle(),
                $student->getLastName(),
                $student->getFirstName(),
                $group,
            ], $this->getStudents(), $this->getGroups()),
        ]);
    }
}

