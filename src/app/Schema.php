<?php

namespace app;

use rotor\Exact;
use rotor\Fallback;
use rotor\Get;
use rotor\Post;
use viewer\View;

class Schema {
    #[Exact, Get('/')]
    public function index() {
        echo View::fromFile('views/Layout.php', [
            'title' => 'Studere | Welcome',
            'body' => View::fromFile('views/SpreadsheetForm.php', [
                'method' => 'POST',
                'action' => '/distribute',
            ]),
        ]);
    }

    #[Exact, Post('/distribute')]
    public function distribute() {
        $groupSize = (int) $_POST['group-size'];
        $format = $_POST['format'];
        $filePath = $_FILES['spreadsheet']['tmp_name'];
        $fileName = $_FILES['spreadsheet']['name'];

        $studentModel = new StudentModel();
        $students = $studentModel->fromSpreadsheet($fileName, $filePath);

        $groupModel = new GroupModel();
        $groups = $groupModel->attributeGroups($students, $groupSize);

        $body = match($format) {
            'chunked'  => new ChunkedDistributedGroupsView($students, $groups),
            default => new CompactDistributedGroupsView($students, $groups),
        };

        echo View::fromFile('views/Layout.php', [
            'title' => 'Studere | Attributed groups',
            'body' => $body,
        ]);
    }

    #[Fallback]
    public function fallback() {
        http_response_code(404);
        echo '404';
    }
}
