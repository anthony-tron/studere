<?php


namespace mapl;

trait Mapable {
    public static function from($object) {
        $T = __CLASS__;
        $reflection = new \ReflectionClass($T);
        $instance = new $T();

        foreach ($reflection->getProperties() as $property) {
            $destinationIdentifier = $property->getName();
            $sourceIdentifier = $property
                ->getAttributes(Identifier::class)[0]
                ->newInstance()
                ->getValue();

            $instance->$destinationIdentifier = $object[$sourceIdentifier] ?? null;
        }

        return $instance;
    }
}
