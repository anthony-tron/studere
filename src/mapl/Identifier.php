<?php

namespace mapl;

#[\Attribute]
class Identifier {
    public function __construct(
        private $value
    )
    {}

    public function getValue() {
        return $this->value;
    }
}